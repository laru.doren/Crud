<script type="text/javascript">
$(document).ready(function(){
	$(':input:not([type="submit"])').each(function() {
		$(this).focus(function() {
			$(this).addClass('hilite');
		}).blur(function() {
			$(this).removeClass('hilite');});
	});	
	
	$("#birth_date").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	
	$("#date_publish").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	
	$("#resign_date").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	
	$("#simpan").click(function(){
		var title 		= $('#title').val();
		var author 		= $('#author').val();
		//var last_name 		= $('#last_name').val();
		//var alias 		= $('#alias').val();
		var string = $("#data").serialize();
		
		if (title.length == 0){
			$.messager.show({
				title:'Info',
				msg:'Sorry, please input Title', 
				timeout:5000,
				showType:'show'
			});
			$('#title').focus();
			return false();
			
		} else if(author.length == 0){
			$.messager.show({
				title:'Info',
				msg:'Sorry, please input Author', 
				timeout:5000,
				showType:'show'
			});
			$('#author').focus();
			return false();
			/*
		} else if(last_name.length == 0){
			$.messager.show({
				title:'Info',
				msg:'Sorry, please input Last Name', 
				timeout:5000,
				showType:'show'
			});
			$('#last_name').focus();
			return false(); */
			/*
		} else if(alias.length == 0){
			$.messager.show({
				title:'Info',
				msg:'Sorry, please input Alias', 
				timeout:5000,
				showType:'show'
			});
			$('#alias').focus();
			return false(); */
			
		}
		
		
		var win = $.messager.progress({
			title:'Please wait',
			msg:'Saving data...',
			text: 'PROCESSING....'
		});
		
		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/employee/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				$.messager.progress('close');
				
				$.messager.show({
					title:'Info',
					msg:data, 
					timeout:2000,
					showType:'slide'
				});
				//CariSimpanan();
			},
			error : function(xhr, teksStatus, kesalahan) {
				$.messager.show({
					title:'Info',
					msg: 'Server not respond :'+kesalahan,
					timeout:2000,
					showType:'slide'
				});
			}
		}); 
		return false();		
	});
	
});	
</script>

<form name="data" id="data">
<fieldset class="atas">
<table width="100%" border="0">
<tr>    
	<td width="150">Title</td>
  <td width="5">:</td>
  <td><input type="text" id="title" name="title" value="<?=$title?>" /><input type="hidden" id="id" name="id" value="<?=$id?>" /></td>
	<td width="400">&nbsp;</td>
	<td width="400">&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>    
	<td width="150">Author</td>
  <td width="5">:</td>
  <td colspan="5"><input type="text" id="author" name="author" value="<?=$author?>" /></td>
</tr>
<tr>    
	<td width="150">Date Published</td>
  <td width="5">:</td>
  <td colspan="5"><input type="text" id="date_publish" name="date_publish" value="<?=$date_publish?>" /></td>
</tr>
<tr>    
	<td width="150">Number of Pages</td>
  <td width="5">:</td>
  <td colspan="5"><input type="text" id="number_pages" name="number_pages" value="<?=$number_pages?>" /></td>
</tr>
<tr>    
	<td width="150">Type of Book</td>
  <td width="5">:</td>
  <td colspan="5">
		<select name="type_book" id="type_book" style="width:150px;">
			<?php foreach($l_func->result() as $func){ 
							if ($func->id==$id){
			?>
								<option value="<?=$func->id?>" selected="selected"><?=$func->description?></option>
			<?php 
							} else {
			?>
								<option value="<?=$func->id?>"><?=$func->description?></option>
			<?php 				
							}
						} 
			?>
		</select>
	</td>
</tr>

<tr>
	<td></td>
	<td></td>
	<td>
    <button type="button" name="simpan" id="simpan" class="easyui-linkbutton" data-options="iconCls:'icon-save'">Save</button>
		<a href="<?php echo base_url();?>index.php">
					<button type="button" name="kembali" id="kembali" class="easyui-linkbutton" data-options="iconCls:'icon-back'">CLOSE</button>
				</a>
	</td>
</tr>
</table>
</fieldset>
</form>