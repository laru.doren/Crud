<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Crud Application</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="imagetoolbar" content="yes">
<meta name="language" content="Indonesia">
<meta name="revisit-after" content="7">
<meta name="webcrawlers" content="all">
<meta name="rating" content="general">
<meta name="spiders" content="all">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/layout.css">
<link href="<?php echo base_url();?>asset/css/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/themes/sunny/easyui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/smoothness/jquery-ui-1.7.2.custom.css">

<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/clock.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.easyui.min.js"></script>

<!--datepicker-->
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/ui.core.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/ui.datepicker-id.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/ui.datepicker.js"></script>

<!--Polling-->
<script type="text/javascript" src="<?php echo base_url();?>asset/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/exporting.js"></script>

<!-- notifikasi -->
<script type="text/javascript" src="<?php echo base_url();?>asset/js/notifikasi.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
		var temp = 1;
    $('#menuaccordion').each(function() {
      var li = $(this);
      var a = $('a', li);
			var url = '<?php echo current_url(); ?>';
			
			if (temp==1){
			//alert(li.accordion('getSelected').toSource());
			//alert(url);
			}
      if(a.href==url) {
				
        li.addClass('active');
      }
			temp++;
    });

	
  });
</script>


<script type="text/javascript">
$(function() {
	$("#dataTable tr:even").addClass("stripe1");
	$("#dataTable tr:odd").addClass("stripe2");
	$("#dataTable tr").hover(
		function() {
			$(this).toggleClass("highlight");
		},
		function() {
			$(this).toggleClass("highlight");
		}
	);
	
	$("#tgl_start").datepicker({
			dateFormat:"yy-mm-dd",
			onClose : function(selectedDate){
				//alert(selectedDate);
				var d = selectedDate.split("-");
				var mindate = new Date(d[0], d[1] - 1, d[2]);
				var maxdate = new Date(d[0], d[1] - 1, +d[2]+13);
				//maxdate.setDate(mindate.getDate()+13);
				$("#tgl_end").datepicker("option","minDate",mindate);
				$("#tgl_end").datepicker("option","maxDate",maxdate);
			}
    });
		
	$("#tgl_end").datepicker({
		dateFormat:"yy-mm-dd",
		onClose : function(selectedDate){
			var d = selectedDate.split("-");
			var maxdate = new Date(d[0], d[1] - 1, d[2]);
			var mindate = new Date(d[0], d[1] - 1, -d[2]-13);
			//mindate.setDate(maxdate.getDate()-13);
			$("#tgl_start").datepicker("option","maxDate",maxdate);
			$("#tgl_start").datepicker("option","minDate",mindate);
		}
	});
	
	$("#backup_tgl_start").datepicker({
			dateFormat:"yy-mm-dd",
			onClose: function(selectedDate){
				var d = selectedDate.split("-");
				var mindate = new Date(d[0], d[1] - 1, d[2]);
				$("#backup_tgl_end").datepicker("option","minDate",mindate);
			}
    });
		
	$("#backup_tgl_end").datepicker({
		dateFormat:"yy-mm-dd",
		onClose: function(selectedDate){
			var d = selectedDate.split("-");
			var maxdate = new Date(d[0], d[1] - 1, d[2]);
			$("#backup_tgl_start").datepicker("option","maxDate",maxdate);
		}
	});
		
	$("#export").click(function(){
			var tglstart = $("#tgl_start").val();
			var tglend = $("#tgl_end").val();
			
			if(tglstart.length == 0){
				var error = true;
				//alert("Sorry, You did not select Start Date");
				$.messager.show({
					title:'Info',
					msg:'Sorry, You did not select Start Date', 
					timeout:5000,
					showType:'show'
				});
				return (false);
			}
			
			if(tglend.length == 0){
				var error = true;
				//alert("Sorry, You did not select To Date");
				$.messager.show({
					title:'Info',
					msg:'Sorry, You did not select To Date', 
					timeout:5000,
					showType:'show'
				});
				return (false);
			}
			
			if (tglstart > tglend){
				var error = true;
				//alert("Sorry, Start Date cannot be Greater than To Date");
				$.messager.show({
					title:'Info',
					msg:'Sorry, Start Date cannot be Greater than To Date', 
					timeout:5000,
					showType:'show'
				});
				return (false);
			}
			 
			//window.open('<?php echo site_url();?>/lap_barang/cetak/'+pilih+'/'+kode+dept+mat_group+'/'+tgl1+'/'+tgl2);
			window.open("<?php echo site_url();?>/brand/cetak/"+tglstart+"/"+tglend);
			return false();
		});
	
});
</script>


<style type="text/css">
 
h1, h2 {
    display: inline-block;
}

ul {list-style: none;padding: 0px;margin: 0px;}
  ul li a:hover {background: #f00;}
  li:hover li {float: none;}
  li:hover a {background: #f00;}
  li:hover li a:hover {background: #000;}
  #drop-nav li ul li {border-top: 0px;}
 

</style>
</head>
<body onLoad="goforit()">
<div class="header" style="height:30px;background:white;padding:2px;margin:0;">	
		<!--<div style="float:left; padding:0px; margin:0px;">
        <img src='<?php echo base_url('asset/images/chakranaga.jpg');?>' style="padding:0; margin:0;" width="285" height="71">
        </div>-->
        <div class="judul" style="float:left; line-height:3px; margin-top:0px; padding:2px 5px;">
        <h1><?php echo $instansi;?></h1>&nbsp;&nbsp;&nbsp;
      <b><?php echo $usaha;?></b>&nbsp;&nbsp;&nbsp;
      <?php echo $alamat_instansi;?>
      </div> 
		<div style="float:right; line-height:3px; text-align:center; margin-top:25px; padding:2px 5px;"> 
        <!--<h1><?php echo $nama_program;?></h1> -->
				<b>Copyright &copy; <?php echo date('Y'); ?></b>
        </div>
	</div>	
	<div class="panel-header" fit="true" style="height:21px;padding-top:1px;text-align:left;">
		
		<div style="float:right; padding-top:5px;">
			<!--<?php echo 'Payroll Period : '.$this->session->userdata('start_date').' to '.$this->session->userdata('end_date');?>&rarr;-->
			<?php echo $this->session->userdata('nama_lengkap');?> &rarr;
            <span id="clock"></span>		
		</div>
	</div>
	<!-- awal kiri -->     
		<div id="tt" class="easyui-tabs" style="height:700px;">
        <div title="<?php echo $judul;?>" style="padding:10px">
		<?php echo $content;?>	
        </div>
    </div>
		
			

</body>
</html>
