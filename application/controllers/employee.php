<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function index()
	{
		/*
		$this->load->helper('cookie');
		if ($_cookie['username']!='' && $_cookie['password']!='' && $_cookie['companyarea']!='')
			$this->app_model->getLoginDataB($_cookie['username'],$_cookie['password'],$_cookie['companyarea']); */
		$cek = $this->session->userdata('logged_in');
		$tahun = date("Y");
		
		//if(!empty($cek)){
			
			 
			
			$d['prg']= $this->config->item('prg');
			$d['web_prg']		= $this->config->item('web_prg');
			
			$d['nama_program']		= $this->config->item('nama_program');
			$d['instansi']				= $this->config->item('instansi');
			$d['usaha']						= $this->config->item('usaha');
			$d['alamat_instansi']	= $this->config->item('alamat_instansi');
			
			$d['judul']	=	"Books";
			
			$page=$this->uri->segment(3);
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$text = "SELECT A.* FROM books A ";		
			$tot_hal = $this->app_model->manualQuery($text);		
			
			$d['tot_hal'] = $tot_hal->num_rows();
			
			$config['base_url'] = site_url();
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$config['next_link'] = 'Next &raquo;';
			$config['prev_link'] = '&laquo; Prev';
			$config['last_link'] = '<b>Last &raquo; </b>';
			$config['first_link'] = '<b> &laquo; First</b>';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			$d['hal'] = $offset;
			
			$text = "SELECT A.*,B.description FROM books A LEFT JOIN type_books B ON B.id = A.type_book 
							LIMIT $limit OFFSET $offset";
			$d['data'] = $this->app_model->manualQuery($text);
			
			$d['content']= $this->load->view('employee/view',$d,true);
			$this->load->view('home',$d); 
	}
	
	public function tambah()
	{
		$cek = $this->session->userdata('logged_in');
		$myheader = $this->session->userdata('myheader');
		
			$d['myheader'] 				= $myheader;
			$d['prg']							= $this->config->item('prg');
			$d['web_prg']					= $this->config->item('web_prg');
			
			$d['nama_program']		= $this->config->item('nama_program');
			$d['instansi']				= $this->config->item('instansi');
			$d['usaha']						= $this->config->item('usaha');
			$d['alamat_instansi']	= $this->config->item('alamat_instansi');

			$d['judul']="Add Books";
			
			$tgl	= date('d-m-Y');
			
			
			$d['title']				= '';
			$d['author']	= '';
			$d['date_publish']		= '';
			$d['number_pages']		= '';
			$d['type_of_book']		= '';
		
			
			$d['content'] = $this->load->view('employee/form', $d, true);		
			$this->load->view('home',$d);
	}
	
	public function edit()
	{
		$cek = $this->session->userdata('logged_in');
		
			//$d['myheader'] 				= $myheader;
			$d['prg']							= $this->config->item('prg');
			$d['web_prg']					= $this->config->item('web_prg');
			
			$d['nama_program']		= $this->config->item('nama_program');
			$d['instansi']				= $this->config->item('instansi');
			$d['usaha']						= $this->config->item('usaha');
			$d['alamat_instansi']	= $this->config->item('alamat_instansi');

			$d['judul']="Edit Crud";
			
			$emp_id_auto = $this->uri->segment(3);
			$d['hal'] = $this->uri->segment(4);
			
			$text = "SELECT * FROM books";
			$data = $this->app_model->manualQuery($text);
			
			if ($data->num_rows() > 0){
				foreach ($data->result() as $db){
					$d['id'] = $db->id;
					$d['title'] = $db->title;
					$d['author'] = $db->author;
					$d['date_publish'] = $db->date_publish;
					$d['number_pages'] = $db->number_pages;
					$d['type_book'] = $db->type_book;
					
				}
			} else {
				$d['id'] = '';
				$d['title'] = '';
				$d['author'] = '';
				$d['date_publish'] = '';
				$d['number_pages'] = '';
				$d['type_book'] = '';
				
			}
			$text = "SELECT * FROM type_books";
			$d['l_func'] = $this->app_model->manualQuery($text);
			
			$d['content'] = $this->load->view('employee/form', $d, true);		
			$this->load->view('home',$d);
		
	}
	
	public function delete(){
		
		$id = $this->uri->segment(3);
						
		$this->app_model->manualQuery("DELETE FROM books WHERE id='$id'");
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."'>";	
		
	}
	
	public function simpan(){
		
			$up['title'] = $this->input->post('title');
			$up['author'] = $this->input->post('author');
			$up['date_publish'] = $this->input->post('date_publish');
			$up['number_pages'] = $this->input->post('number_pages');
			$up['type_book'] = $this->input->post('type_book');
			
			$id['id'] = $this->input->post('id');
			
			$data = $this->app_model->getSelectedData("books",$id);
			
			if($data->num_rows()>0){
				$this->app_model->updateData("books",$up,$id);
				
				echo "Update Successful!";
			} else{
				$this->app_model->insertData("books",$up);
				echo "Save Successful!";
			}
			
	}
	
}

/* End of file employee.php */
/* Location: ./application/controllers/employee.php */