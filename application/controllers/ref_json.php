<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ref_json extends CI_Controller {

	public function CariNoSJ(){
		$bln = date('m');
		$th = date('y');
		$text = "SELECT max(kodebeli) as no FROM h_beli";
		$data = $this->app_model->manualQuery($text);
		if($data->num_rows() > 0 ){
			foreach($data->result() as $t){
				$no = $t->no; 
				$tmp = ((int) substr($no,5,5))+1;
				$hasil = 'BL'.sprintf("%05s", $tmp);
			}
		}else{
			$hasil = 'BL'.'00001';
		}
		return $hasil;
	}
	
	public function InfoBarang()
	{
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$kode = $this->input->post('kode');
			$text = "SELECT barang.kode_barang, barang.nama_barang, unit.unit_name, departemen_fact.dept_name 
						FROM barang 
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen_fact ON departemen_fact.dept_code=barang.departemen
						WHERE barang.kode_barang='$kode'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['nama_barang'] = $t->nama_barang;
					$data['satuan'] = $t->unit_name;
					$data['departemen'] = $t->dept_name;
					echo json_encode($data);
				}
			}else{
					$data['nama_barang'] = '';
					$data['satuan'] = '';
					$data['departemen'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function GeneratePo(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$th = date('y');
			$lilo = $this->input->post('lilo');
			$bin = strtoupper($lilo);
			
			if($bin=="LOC") {
				$local = '1';
			}
			else {
				$local = '2';
			}
			
			$text = "SELECT MAX(po) as no FROM h_beli WHERE po LIKE '__".$local."____'";

			$table = $this->app_model->manualQuery($text);
			if($table->num_rows() > 0) {
				foreach($table->result() as $row) {
					$no = $row->no;
					$tmp = ((int) substr($no,4,4))+1;
					$data['hasil'] = $th.$local.sprintf("%04s", $tmp);
					$text2 = "INSERT INTO h_beli (po) VALUES ('".$data['hasil']."')";
					//$text3 = "INSERT INTO d_beli (po) VALUES ('".$data['hasil']."')";
					$this->app_model->manualQuery($text2);
					//$this->app_model->manualQuery($text3);
					echo json_encode($data);
				}
			}
			else {
				$data['hasil'] = $th.$local.'0001';
				$text2 = "INSERT INTO h_beli (po) VALUES ('".$data['hasil']."')";
				//$text3 = "INSERT INTO d_beli (po) VALUES ('".$data['hasil']."')";
				$this->app_model->manualQuery($text2);
				//$this->app_model->manualQuery($text3);
				echo json_encode($data);
			}
			
		}else{
			header('location:'.base_url());
		}
	}
	
	public function InfoTerima()
	{
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$kode = $this->input->post('kode');
			$text = "SELECT barang.kode_barang, barang.nama_barang, unit.unit_name, departemen_fact.dept_name
						FROM barang 
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen_fact ON departemen_fact.dept_code=barang.departemen
						WHERE barang.kode_barang='$kode'";
			$tabel = $this->app_model->manualQuery($text);
			
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['nama_barang'] = $t->nama_barang;
					$data['satuan'] = $t->unit_name;
					$data['departemen'] = $t->dept_name;
					echo json_encode($data);
				}
			}else{
					$data['nama_barang'] = '';
					$data['satuan'] = '';
					$data['departemen'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function InfoSupplier()
	{
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$kode = $this->input->post('kode');
			$text = "SELECT * FROM supplier WHERE supplier_code='$kode'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['tgl'] = $t->supplier_date;
					$data['nama'] = $t->supplier_name;
					$data['alamat'] = $t->supplier_address;
					$data['negara'] = $t->supplier_state;
					$data['telp'] = $t->supplier_phone;
					$data['fax'] = $t->supplier_fax;
					$data['mobile'] = $t->supplier_mobile;
					$data['email'] = $t->supplier_email;
					echo json_encode($data);
				}
			}else{
				$data['tgl'] = '';
				$data['nama'] = '';
				$data['alamat'] = '';
				$data['negara'] = '';
				$data['telp'] = '';
				$data['fax'] = '';
				$data['mobile'] = '';
				$data['email'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function InfoCustomer()
	{
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$kode = $this->input->post('kode');
			$text = "SELECT A.cust_name,A.cust_phone,A.cust_fax,A.cust_mobile,A.cust_email,B.negara FROM customer A LEFT OUTER JOIN negara B ON B.negara_code=A.cust_country WHERE A.cust_code='$kode'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['nama'] = urldecode($t->cust_name);
					$data['negara'] = $t->negara;
					$data['telp'] = $t->cust_phone;
					$data['fax'] = $t->cust_fax;
					$data['mobile'] = $t->cust_mobile;
					$data['email'] = $t->cust_email;
					echo json_encode($data);
				}
			}else{
				$data['tgl'] = '';
				$data['nama'] = '';
				$data['alamat'] = '';
				$data['negara'] = '';
				$data['telp'] = '';
				$data['fax'] = '';
				$data['mobile'] = '';
				$data['email'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataBarang(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			
			$cari= $this->input->post('cari');
			/*if(empty($cari)){
				$text = "SELECT barang.kode_barang, barang.kode_lama, barang.nama_barang, unit.unit_name, departemen.dept_name
						FROM barang
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen ON departemen.dept_code=barang.departemen
						ORDER BY barang.kode_barang";
			}else{*/
				$text = "SELECT barang.kode_barang, barang.kode_lama, barang.nama_barang, unit.unit_name, departemen.dept_name
						FROM barang
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen ON departemen.dept_code=barang.departemen
						WHERE kode_barang LIKE '%$cari%' OR kode_lama LIKE '%$cari%' OR nama_barang LIKE '%$cari%'";
			//}
			$d['data'] = $this->app_model->manualQuery($text);
			//$d['data'] = $this->app_model->prepare($text);
			
			$this->load->view('data_barang',$d);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataBarang1(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			
			if (!empty($this->input->get_post('q'))){
				$cari = $this->input->get_post('q');
			} else {
				$cari = '';
			}
			$text = "SELECT barang.kode_barang, barang.kode_lama, barang.nama_barang, unit.unit_name, departemen_fact.dept_name
					FROM barang
					LEFT OUTER JOIN unit ON unit.unit_code=barang.satuan
					LEFT OUTER JOIN departemen_fact ON departemen_fact.dept_code=barang.departemen
					WHERE kode_barang LIKE '%$cari%' OR kode_lama LIKE '%$cari%' OR nama_barang LIKE '%$cari%'";
			$tabel = $this->app_model->manualQuery($text);
			
			$row =$tabel->num_rows();
			$temp = 0;
			if ($row>0){
				foreach($tabel->result() as $t){
					$data[$temp]['kode_barang'] = $t->kode_barang;
					$data[$temp]['kode_lama'] = $t->kode_lama;
					$data[$temp]['nama_barang'] = $t->nama_barang;
					$data[$temp]['unit_name'] = $t->unit_name;
					$data[$temp]['dept_name'] = $t->dept_name;
					$temp++;
				}
			} else {
				$data[$temp]['kode_barang'] = '';
				$data[$temp]['kode_lama'] = '';
				$data[$temp]['nama_barang'] = '';
				$data[$temp]['unit_name'] = '';
				$data[$temp]['dept_name'] = '';
			}
			//$d['data'] = $this->app_model->manualQuery($text);
			echo '{"total":'.$row.',"rows":'.json_encode($data).'}';
			//$this->load->view('data_barang',$d);
		}else{
			header('location:'.base_url());
		}
	}
	
	
	public function DataCustomer(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$cari= $this->input->post('cari');
			if(empty($cari)){
				$text = "SELECT * FROM customer WHERE cust_code<>'000'";
			}else{
				$text = "SELECT * FROM customer	WHERE cust_code LIKE '%$cari%' OR cust_name LIKE '%$cari%' AND cust_code<>'000'";
			}
			$d['data'] = $this->app_model->manualQuery($text);
			
			$this->load->view('data_customer',$d);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataTerima(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$cari= $this->input->post('cari');
			if(empty($cari)){
				$text = "SELECT barang.kode_barang, barang.nama_barang, unit.unit_name, departemen.dept_name
						FROM barang
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen ON departemen.dept_code=barang.departemen
						ORDER BY barang.kode_barang";
			}else{
				$text = "SELECT barang.kode_barang, barang.nama_barang, unit.unit_name, departemen.dept_name
						FROM barang
						JOIN unit ON unit.unit_code=barang.satuan
						JOIN departemen ON departemen.dept_code=barang.departemen
						WHERE kode_barang LIKE '%$cari%' OR nama_barang LIKE '%$cari%'";
			}
			$d['data'] = $this->app_model->manualQuery($text);
			
			$this->load->view('data_terima',$d);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataPropinsi(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$negara_code= $this->input->get('kode');
			$text = "SELECT propinsi_code,propinsi from propinsi where negara_code='$negara_code' ORDER BY Propinsi";
			
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['propinsi_code'] = $t->propinsi_code;
					$data['propinsi'] = $t->propinsi;
					echo json_encode($data);
				}
			}else{
				$data['propinsi_code'] = '';
				$data['propinsi'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataKota(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$propinsi_code= $this->input->post('kode');
			$text = "SELECT kota_code,kota from kota where propinsi_code='$propinsi_code' ORDER BY Kota";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['kota_code'] = $t->kota_code;
					$data['kota'] = $t->kota;
					echo json_encode($data);
				}
			}else{
				$data['kota_code'] = '';
				$data['kota'] = '';
				echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataNegara(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$text = "SELECT negara_code,negara from negara ORDER BY negara";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['negara_code'] = $t->negara_code;
					$data['negara'] = $t->negara;
					//echo json_encode($data);
				}
			}else{
				$data['negara_code'] = '';
				$data['negara'] = '';
				//echo json_encode($data);
			}
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataMaterial(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$kode = $this->input->post('kode');
			$text = "SELECT B.family FROM barang A LEFT OUTER JOIN family B on B.family_id=A.family where A.kode_barang='$kode'";
			$tabel = $this->app_model->manualQuery($text);
			
			$row = $tabel->num_rows();
			if($row>0){
				foreach($tabel->result() as $t){
					$data['family'] = $t->family;
				}
			}else{
				$data['family'] = '';
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataMaterial1(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$q = isset($_POST['q']) ? strval($_POST['q']) : '';
			$dept = $this->uri->segment(3);
			if (empty($dept))
				$on = "";
			else
				$on =" and E.$dept='1'";
			$kode = $this->uri->segment(4);
			if (empty($kode) OR $kode=='null')
				$tempKode = "";
			else
				$tempKode = " and A.family='$kode'";
			if ($dept=='upholstery'){
				$text = "SELECT distinct(A.kode_barang),A.nama_barang,A.family,E.family as family_name,A.average_waste,A.brg_harga,C.currency_name,C.rates,D.convertion,D.convertion_hrg,D.unit_name,A.size_length,A.size_width,A.size_height,F.simbol as length_unit,G.type as tipe,A.waste_log_plank,A.waste_plank_raw,A.waste_raw_comp FROM barang A LEFT OUTER JOIN departemen_fact B on B.dept_code=A.departemen LEFT OUTER JOIN currency C on c.currency_id=A.brg_currency LEFT OUTER JOIN `unit` D on D.unit_code=A.satuan LEFT OUTER JOIN family E on E.family_id=A.family $on LEFT OUTER JOIN t_unit_ukuran F on F.unit_ukuran_id=A.size_length_unit LEFT OUTER JOIN `type` G on G.type_id=A.`type` where (A.nama_barang!='' OR A.nama_barang_eng!='') and A.nama_barang like '%$q%' and B.dept_code='UPHOLD' $tempKode";
			} else if ($dept=='wood'){
				$text = "SELECT distinct(A.kode_barang),A.nama_barang,A.family,E.family as family_name,A.average_waste,A.brg_harga,C.currency_name,C.rates,D.convertion,D.convertion_hrg,D.unit_name,A.size_density as size_length,A.size_width,A.size_height,F.simbol as length_unit,G.type as tipe,A.waste_log_plank,A.waste_plank_raw,A.waste_raw_comp FROM barang A LEFT OUTER JOIN departemen_fact B on B.dept_code=A.departemen LEFT OUTER JOIN currency C on c.currency_id=A.brg_currency LEFT OUTER JOIN `unit` D on D.unit_code=A.satuan LEFT OUTER JOIN family E on E.family_id=A.family LEFT OUTER JOIN t_unit_ukuran F on F.unit_ukuran_id=A.size_density_unit LEFT OUTER JOIN `type` G on G.type_id=A.`type` where (A.nama_barang!='' OR A.nama_barang_eng!='') and A.nama_barang like '%$q%' and B.dept_name='Wood'";
			} else if ($dept=='glue'){
				$text = "SELECT distinct(A.kode_barang),A.nama_barang,A.family,E.family as family_name,A.average_waste,A.brg_harga,C.currency_name,C.rates,D.convertion,D.convertion_hrg,D.unit_name,A.size_density as size_length,A.size_width,A.size_height,F.simbol as length_unit,G.type as tipe,A.waste_log_plank,A.waste_plank_raw,A.waste_raw_comp FROM barang A LEFT OUTER JOIN departemen_fact B on B.dept_code=A.departemen LEFT OUTER JOIN currency C on c.currency_id=A.brg_currency LEFT OUTER JOIN `unit` D on D.unit_code=A.satuan LEFT OUTER JOIN family E on E.family_id=A.family LEFT OUTER JOIN t_unit_ukuran F on F.unit_ukuran_id=A.size_density_unit LEFT OUTER JOIN `type` G on G.type_id=A.`type` where (A.nama_barang!='' OR A.nama_barang_eng!='') and A.nama_barang like '%$q%' and E.family='Glue'";
			} else if ($dept=='panel'){
				$text = "SELECT distinct(A.kode_barang),A.nama_barang,A.family,E.family as family_name,A.average_waste,A.brg_harga,C.currency_name,C.rates,D.convertion,D.convertion_hrg,D.unit_name,A.size_length,A.size_width,A.size_height,F.simbol as length_unit,G.type as tipe,A.waste_log_plank,A.waste_plank_raw,A.waste_raw_comp FROM barang A LEFT OUTER JOIN departemen_fact B on B.dept_code=A.departemen LEFT OUTER JOIN currency C on c.currency_id=A.brg_currency LEFT OUTER JOIN `unit` D on D.unit_code=A.satuan LEFT OUTER JOIN family E on E.family_id=A.family LEFT OUTER JOIN t_unit_ukuran F on F.unit_ukuran_id=A.size_length_unit LEFT OUTER JOIN `type` G on G.type_id=A.`type` where (A.nama_barang!='' OR A.nama_barang_eng!='') and A.nama_barang like '%$q%' and B.dept_name='Panel'";
			} else if ($dept=='accessories'){
				$text = "SELECT distinct(A.kode_barang),A.nama_barang,A.family,E.family as family_name,A.average_waste,A.brg_harga,C.currency_name,C.rates,D.convertion,D.convertion_hrg,D.unit_name,A.size_length,A.size_width,A.size_height,F.simbol as length_unit,G.type as tipe,A.waste_log_plank,A.waste_plank_raw,A.waste_raw_comp FROM barang A LEFT OUTER JOIN departemen_fact B on B.dept_code=A.departemen LEFT OUTER JOIN currency C on c.currency_id=A.brg_currency LEFT OUTER JOIN `unit` D on D.unit_code=A.satuan LEFT OUTER JOIN family E on E.family_id=A.family LEFT OUTER JOIN t_unit_ukuran F on F.unit_ukuran_id=A.size_length_unit LEFT OUTER JOIN `type` G on G.type_id=A.`type` where (A.nama_barang!='' OR A.nama_barang_eng!='') and A.nama_barang like '%$q%' and B.dept_name LIKE '%Accessories%'";
			}
			$tabel = $this->app_model->manualQuery($text);
			$fp=fopen("dataupholstery.txt","w");
			fwrite($fp,$text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['kode_barang'] = $t->kode_barang;
					$data[$temp]['nama_barang'] = $t->nama_barang;
					$data[$temp]['family'] = $t->family;
					$data[$temp]['family_name'] = $t->family_name;
					$data[$temp]['average_waste'] = $t->average_waste;
					$data[$temp]['brg_harga'] = $t->brg_harga;
					$data[$temp]['currency_name'] = $t->currency_name;
					$data[$temp]['size_length'] = $t->size_length;
					$data[$temp]['size_width'] = $t->size_width;
					$data[$temp]['size_height'] = $t->size_height;
					$data[$temp]['unit_name'] = $t->unit_name;
					$data[$temp]['length_unit'] = $t->length_unit;
					$data[$temp]['type'] = $t->tipe;
					$data[$temp]['rates'] = $t->rates;
					$data[$temp]['waste_log_plank'] = $t->waste_log_plank;
					$data[$temp]['waste_plank_raw'] = $t->waste_plank_raw;
					$data[$temp]['waste_raw_comp'] = $t->waste_raw_comp;
					$temp++;
				}
			}else{
				$data[0]['kode_barang'] = '';
				$data[0]['nama_barang'] = '';
				$data[0]['family'] = '';
				$data[0]['family_name'] = '';
				$data[0]['average_waste'] = '';
				$data[0]['brg_harga'] = '';
				$data[0]['currency_name'] = '';
				$data[0]['size_length'] = '';
				$data[0]['size_width'] = '';
				$data[0]['size_height'] = ''; 
				$data[0]['unit_name'] = '';
				$data[0]['length_unit'] = '';
				$data[0]['type'] = '';
				$data[0]['rates'] = '';
				$data[0]['waste_log_plank'] = '';
				$data[0]['waste_plank_raw'] = '';
				$data[0]['waste_raw_comp'] = '';
			}
			
			echo json_encode($data);
			
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataFamily(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$dept = $this->uri->segment(3);
			
			
			$textdept = "SELECT dept_code from departemen_fact where dept_name like '%".ucfirst($dept)."%' and companyarea='$loc'";
			$tabeldept = $this->app_model->manualQuery($textdept);
			
			$tmprows = $tabeldept->num_rows();
			if ($tmprows>0){
				foreach($tabeldept->result() as $t){
					$tempdept =  $t->dept_code;
				}
			} else{
				$tempdept = '';
			}
			if (!empty($tempdept))
				$where = "where departemen like '%$tempdept%' and companyarea='$loc'";
			else
				$where ="where departemen='$tempdept' and departemen!='' and companyarea='$loc'";
			
			$text = "SELECT A.family_id,A.family FROM family A $where";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['family_id'] = $t->family_id;
					$data[$temp]['family'] = $t->family;
					$temp++;
				}
			}else{
				$data[0]['family_id'] = '';
				$data[0]['family'] = '';
			}
			
			echo json_encode($data);
			
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxCustomer(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$text = "SELECT A.cust_code,A.cust_name,B.negara FROM customer A LEFT OUTER JOIN negara B ON B.negara_code=A.cust_country where A.companyarea='$loc'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['cust_code'] = $t->cust_code;
					$data[$temp]['cust_name'] = $t->cust_name;
					$data[$temp]['negara'] = $t->negara;
					$temp++;
				}
			}else{
				$data[0]['cust_code'] = '';
				$data[0]['cust_name'] = '';
				$data[0]['negara'] = '';
			}
			
			echo json_encode($data);
			
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxLayer(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$dept = $this->uri->segment(3);
			$text = "SELECT A.layer_id,A.layer FROM layer A where A.companyarea='$loc' and $dept='1'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['layer_id'] = $t->layer_id;
					$data[$temp]['layer'] = $t->layer;
					$temp++;
				}
			}else{
				$data[0]['layer_id'] = '';
				$data[0]['layer'] = '';
			}
			
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxDepartemen(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$text = "SELECT A.dept_code,A.dept_name FROM departemen_fact A where A.companyarea='$loc'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['dept_code'] = $t->dept_code;
					$data[$temp]['dept_name'] = $t->dept_name;
					$temp++;
				}
			}else{
				$data[0]['dept_code'] = '';
				$data[0]['dept_name'] = '';
			}
			
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataType(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$dept = $this->uri->segment(3);
			
			$textdept = "SELECT dept_code from departemen_fact where dept_name like '%".ucfirst($dept)."%' and companyarea='$loc'";
			$tabeldept = $this->app_model->manualQuery($textdept);

			$tmprows = $tabeldept->num_rows();
			if ($tmprows>0){
				foreach($tabeldept->result() as $t){
					$tempdept =  $t->dept_code;
				}
			} else{
				$tempdept = '';
			}
			//if (!empty($dept))
				$where = "where departemen like '%$tempdept%' and companyarea='$loc'";
			//else
			//	$where ="where companyarea='$loc'";
				
			$text = "SELECT A.type_id,A.type FROM `type` A $where ";
			$tabel = $this->app_model->manualQuery($text);
			
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['type_id'] = $t->type_id;
					$data[$temp]['type'] = $t->type;
					$temp++;
				}
			}else{
				$data[0]['type_id'] = '';
				$data[0]['type'] = '';
			}
			
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataTerimaBarang(){
		$cek = $this->session->userdata('logged_in');
		if(!empty($cek)){
			$dept = $this->input->post('departemen');
			if (empty($dept)){
				$dept = '';
			}
			$cari= $this->input->post('cari');
			if (empty($cari)){
				$cari='';
				$limit = ' LIMIT 50 OFFSET 0';
			} else {
				$limit = '';
			}
			/*
			$page=$this->input->post('limit');
			$limit=$this->config->item('limit_data');
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$text = "SELECT barang.kode_barang, barang.kode_lama, barang.nama_barang, unit.unit_name, departemen_fact.dept_name
					FROM barang
					LEFT OUTER JOIN unit ON unit.unit_code=barang.satuan
					LEFT OUTER JOIN departemen_fact ON departemen_fact.dept_code=barang.departemen
					WHERE departemen_fact.dept_code='$dept' and (kode_barang LIKE '%$cari%' OR kode_lama LIKE '%$cari%' OR nama_barang LIKE '%$cari%')
					ORDER BY barang.nama_barang";
			$tot_hal = $this->app_model->manualQuery($text);		
			$d['tot_hal'] = $tot_hal->num_rows();
			
			$config['base_url'] = "javascript:halaman($page)";
			$config['total_rows'] = $tot_hal->num_rows();
			$config['per_page'] = $limit;
			//$config['uri_segment'] = 3;
			$config['next_link'] = 'Lanjut &raquo;';
			$config['prev_link'] = '&laquo; Kembali';
			$config['last_link'] = '<b>Terakhir &raquo; </b>';
			$config['first_link'] = '<b> &laquo; Pertama</b>';
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			$d['hal'] = $offset; */
			
			$text = "SELECT barang.kode_barang, barang.kode_lama, barang.nama_barang, unit.unit_name, departemen_fact.dept_name
					FROM barang
					LEFT OUTER JOIN unit ON unit.unit_code=barang.satuan
					LEFT OUTER JOIN departemen_fact ON departemen_fact.dept_code=barang.departemen
					WHERE departemen_fact.dept_code='$dept' and (kode_barang LIKE '%$cari%' OR kode_lama LIKE '%$cari%' OR nama_barang LIKE '%$cari%')
					ORDER BY barang.nama_barang
					$limit";
			$d['data'] = $this->app_model->manualQuery($text);
			$this->load->view('data_barang1',$d);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataTypeMaterial(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$tipe = $this->uri->segment(3);
			
			$textdept = "SELECT dept_code from departemen_fact where dept_name like '%".ucfirst($tipe)."%' and companyarea='$loc'";
			$tabeldept = $this->app_model->manualQuery($textdept);

			$tmprows = $tabeldept->num_rows();
			if ($tmprows>0){
				foreach($tabeldept->result() as $t){
					$tempdept =  $t->dept_code;
				}
			} else{
				$tempdept = '';
			}
			
			$where = "where departemen='$tempdept' and companyarea='$loc'";
			
			$text = "SELECT A.kode_barang,A.nama_barang FROM barang A $where ";
			$tabel = $this->app_model->manualQuery($text);
			$data = array();
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['kode_material'] = $t->kode_barang;
					$data[$temp]['nama_material'] = $t->nama_barang;
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxProduction(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$text = "SELECT * FROM production_type where companyarea='$loc'";
			$tabel = $this->app_model->manualQuery($text);
			$data = array();
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['production_id'] = $t->production_id;
					$data[$temp]['production'] = $t->production;
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxProduct(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		if(!empty($cek)){
			$category = $this->uri->segment(3);
			
			$q = isset($_POST['q']) ? strval($_POST['q']) : '';
			$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
			$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
			$offset = ($page-1)*$rows;
			$data = array();
			$text = "SELECT count(*) as total FROM product A LEFT OUTER JOIN collection B ON B.coll_code=A.product_coll where A.companyarea='$loc' and A.category='$category' and A.product_code like '%$q%'";
			$tabel = $this->app_model->manualQuery($text);
			$vrow =1 ;
			foreach($tabel->result() as $tb){
				$vrow = $tb->total;
			}
			
			$text = "SELECT A.product_code,B.coll_name,A.product_name,A.category FROM product A LEFT OUTER JOIN collection B ON B.coll_code=A.product_coll where A.companyarea='$loc' and A.category='$category' and A.product_code like '%$q%' limit $rows OFFSET $offset";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['product_code'] = htmlentities($t->product_code);
					$data[$temp]['coll_name'] = htmlentities($t->coll_name);
					$data[$temp]['product_name'] = htmlentities($t->product_name);
					$data[$temp]['category'] = htmlentities($t->category);
					$temp++;
				}
			}
			echo '{"total":'.$vrow.',"rows":'.json_encode($data).'}';
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxEmployee(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$q = isset($_POST['q']) ? strval($_POST['q']) : '';
			$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
			$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
			$offset = ($page-1)*$rows;
			$data = array();
			$text = "SELECT count(*) as total FROM emp A LEFT OUTER JOIN dept B ON B.dept_id_auto=A.dept_id_auto where (A.alias like '%$q%' OR B.level_id like '%$q%') AND (A.resign_date='0000-00-00' OR A.resign_date IS NULL)";
			$tabel = $this->app_model->manualQuery($text);
			$vrow =1 ;
			foreach($tabel->result() as $tb){
				$vrow = $tb->total;
			}
			
			$text = "SELECT A.nik,A.alias,A.gender,B.level_id FROM emp A LEFT OUTER JOIN dept B ON B.dept_id_auto=A.dept_id_auto where (A.alias like '%$q%' OR B.level_id like '%$q%') AND (A.resign_date='0000-00-00' OR A.resign_date IS NULL) ORDER BY B.level_id,A.nik limit $rows OFFSET $offset";
			$tabel = $this->app_model->manualQuery($text);
			
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['nik'] = $t->nik;
					$data[$temp]['nama'] = $t->alias;
					if ($t->gender=='1'){
						$data[$temp]['gender'] = 'F';
					}else {
						$data[$temp]['gender'] = 'M';
					}
					
					$data[$temp]['departement'] = $t->level_id;
					$temp++;
				}
			}
			echo '{"total":'.$vrow.',"rows":'.json_encode($data).'}';
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxAllMaterial(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$q = isset($_POST['q']) ? strval($_POST['q']) : '';
			$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
			$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
			$offset = ($page-1)*$rows;
			$data = array();
			$text = "SELECT count(*) as total FROM barang A where A.companyarea='$loc' and A.nama_barang like '%$q%'";
			$tabel = $this->app_model->manualQuery($text);
			$vrow =1 ;
			foreach($tabel->result() as $tb){
				$vrow = $tb->total;
			}
			
			$text = "SELECT A.kode_barang,A.nama_barang,A.kode_barang_spc,A.size_length,A.size_width,A.size_height,A.size_diameter,A.size_diameterin,B.unit_name FROM barang A LEFT OUTER JOIN unit B ON B.unit_code=A.satuan where A.companyarea='$loc' and A.nama_barang like '%$q%' limit $rows OFFSET $offset";
			$tabel = $this->app_model->manualQuery($text);
			
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['kode_barang'] = $t->kode_barang;
					$data[$temp]['nama_barang'] = htmlentities($t->nama_barang);
					$data[$temp]['kode_barang_spc'] = $t->kode_barang_spc;
					$data[$temp]['length'] = number_format($t->size_length,0);
					$data[$temp]['width'] = number_format($t->size_width,0);
					$data[$temp]['height'] = number_format($t->size_height,0);
					$data[$temp]['unit_name'] = $t->unit_name;
					$data[$temp]['diameter'] = $t->size_diameter;
					$data[$temp]['diameterin'] = $t->size_diameterin;
					$temp++;
				}
			}
			echo '{"total":'.$vrow.',"rows":'.json_encode($data).'}';
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxShift(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$text = "SELECT * FROM shift_l";
			$tabel = $this->app_model->manualQuery($text);
			$data = array();
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['shift_id'] = $t->shift_id;
					$data[$temp]['kode'] = $t->kode;
					$data[$temp]['shift_name'] = $t->shift_name;
					$data[$temp]['shift_des'] = $t->kode.' || '.$t->shift_name;
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
		
	}
	
	public function CombogridGroupShift(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$q = isset($_POST['q']) ? strval($_POST['q']) : '';
			$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
			$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
			$offset = ($page-1)*$rows;
			$data = array();
			
			$text ="SELECT A.schedule_id,A.schedule_name,A.cycles
					FROM schedule_l A
					WHERE A.schedule_name like '%$q%'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['schedule_id'] = $t->schedule_id;
					$data[$temp]['schedule_name'] = $t->schedule_name;
					$data[$temp]['cycles'] = $t->cycles;
					//$data[$temp]['shift_des'] = $t->kode.' || '.$t->shift_name;
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataPotongan(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$data = array();
			$text = "SELECT * FROM payroll_potongan WHERE companyarea='$loc'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['potongan_id'] = $t->potongan_id;
					$data[$temp]['nama_potongan'] = $t->nama_potongan;
					
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function DataTambahan(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$data = array();
			$text = "SELECT * FROM payroll_tambahan WHERE companyarea='$loc'";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['tambahan_id'] = $t->tambahan_id;
					$data[$temp]['nama_tambahan'] = $t->nama_tambahan;
					
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
	}
	
	public function ComboboxPermit(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$data = array();
			$text = "SELECT * FROM master_permit WHERE companyarea='$loc' ORDER BY permit_name";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['permit_id'] = $t->permit_id;
					$data[$temp]['permit_name'] = $t->permit_name;
					$data[$temp]['penalty'] = '';
					if ($t->penalty==1){
						$data[$temp]['penalty'] = 'YES';
					}
					$data[$temp]['full_pay'] = '';
					if ($t->full_pay==1){
						$data[$temp]['full_pay'] = 'YES';
					}
					$data[$temp]['half_pay'] = '';
					if ($t->half_pay==1){
						$data[$temp]['half_pay'] = 'YES';
					}
					$data[$temp]['min_leave'] = '';
					if ($t->min_leave==1){
						$data[$temp]['min_leave'] = 'YES';
					}
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
		
	}
	
	public function ComboboxSpecialLeave(){
		$cek = $this->session->userdata('logged_in');
		$loc = $this->session->userdata('companyarea');
		
		if(!empty($cek)){
			$data = array();
			$text = "SELECT * FROM master_specialleave WHERE companyarea='$loc' ORDER BY specialleave_name";
			$tabel = $this->app_model->manualQuery($text);
			$row = $tabel->num_rows();
			if($row>0){
				$temp=0;
				foreach($tabel->result() as $t){
					$data[$temp]['specialleave_id'] = $t->specialleave_id;
					$data[$temp]['specialleave_name'] = $t->specialleave_name;
					$temp++;
				}
			}
			echo json_encode($data);
		}else{
			header('location:'.base_url());
		}
		
	}
	
}

/* End of file ref_json.php */
/* Location: ./application/controllers/ref_json.php */