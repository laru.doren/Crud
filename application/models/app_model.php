<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Model extends CI_Model {

	/**
	 * @author : Deddy Rusdiansyah
	 * @web : http://deddyrusdiansyah.blogspot.com
	 * @keterangan : Model untuk menangani semua query database aplikasi
	 **/
	
	
	
	public function getAllData($table)
	{
		return $this->db->get($table);
	}
	
	public function getAllDataLimited($table,$limit,$offset)
	{
		return $this->db->get($table, $limit, $offset);
	}
	
	public function getSelectedDataLimited($table,$data,$limit,$offset)
	{
		return $this->db->get_where($table, $data, $limit, $offset);
	}
		
	//select table
	public function getSelectedData($table,$data)
	{
		return $this->db->get_where($table, $data);
	}
	
	//update table
	function updateData($table,$data,$field_key)
	{
		if(!$this->db->update($table,$data,$field_key)){
			echo 'Update Data Failed!';
		} else{
			echo 'Update Data Successful!';
		}
	}
	
	function deleteData($table,$data)
	{
		$this->db->delete($table,$data);
	}
	
	function insertData($table,$data)
	{
		if (!$this->db->insert($table,$data)){
			$data = 'Save Data Failed!';
		} else {
			$data = 'Save Data Successful!';
		}
		return $data;
	}
	
	//Query manual
	function manualQuery($q)
	{
		return $this->db->query($q);
	}
	
	public function myaccess($id){
		$user = $this->session->userdata('username');
		$loc = $this->session->userdata('companyarea');
		
		$text = "SELECT A.`view` 
				 FROM otorisasi A 
				 LEFT OUTER JOIN menusource B on B.companyarea=A.companyarea and B.menuid=A.menuid 
				 WHERE A.companyarea='$loc' and B.linkchild='$id' and A.OPERATORID='$user' and A.`view`='1'";
		$d = $this->app_model->manualQuery($text);
		$r = $d->num_rows();
		if ($r>0)
			{$hasil ='1';}
		else
		{ $hasil='0';}
		return $hasil;
	}
	
	public function CariLevel($id){
		$t = "SELECT * FROM level WHERE id_level='$id'";
		$d = $this->app_model->manualQuery($t);
		$r = $d->num_rows();
		if($r>0){
			foreach($d->result() as $h){
				$hasil = $h->level;
			}
		}else{
			$hasil = '';
		}
		return $hasil;
	}
		
	//Konversi tanggal
	public function tgl_sql($date){
		$exp = explode('-',$date);
		if(count($exp) == 3) {
			$date = $exp[2].'-'.$exp[1].'-'.$exp[0];
		}
		return $date;
	}
	public function tgl_str($date){
		$exp = explode('-',$date);
		if(count($exp) == 3) {
			$date = $exp[2].'-'.$exp[1].'-'.$exp[0];
		}
		return $date;
	}
	
	public function ambilTgl($tgl){
		$exp = explode('-',$tgl);
		$tgl = $exp[2];
		return $tgl;
	}
	
	public function ambilBln($tgl){
		$exp = explode('-',$tgl);
		$tgl = $exp[1];
		$bln = $this->app_model->getBulan($tgl);
		$hasil = substr($bln,0,3);
		return $hasil;
	}
	
	public function tgl_indo($tgl){
			$jam = substr($tgl,11,10);
			$tgl = substr($tgl,0,10);
			$tanggal = substr($tgl,8,2);
			$bulan = $this->app_model->getBulan(substr($tgl,5,2));
			$tahun = substr($tgl,0,4);
			return $tanggal.' '.$bulan.' '.$tahun.' '.$jam;		 
	}	

	public function getBulan($bln){
		switch ($bln){
			case 1: 
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	} 
	
	public function hari_ini($hari){
		date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
		$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
		//$hari = date("w");
		$hari_ini = $seminggu[$hari];
		return $hari_ini;
	}
	
	//query login
	public function getLoginData($usr,$psw,$loc)
	{
		
		$u = mysql_real_escape_string($usr);
		$p = md5(mysql_real_escape_string($psw));
		$q_cek_login = $this->db->get_where('admins', array('username' => $u, 'password' => $p, 'companyarea' => $loc));

		if(count($q_cek_login->result())>0)
		{
			foreach($q_cek_login->result() as $qck)
			{
					foreach($q_cek_login->result() as $qad)
					{
						$text = "SELECT A.start_date,A.end_date FROM payroll_period A where A.`status`='1'";
						$dataperiod = $this->app_model->manualQuery($text);
						$resultperiod = $dataperiod->row();
						$sess_data['start_date'] = $resultperiod->start_date;
						$sess_data['end_date'] = $resultperiod->end_date;
						
						$sess_data['logged_in'] = 'aingLoginYeuh';
						$sess_data['username'] = $qad->username;
						$sess_data['nama_lengkap'] = $qad->nama_lengkap;
						$sess_data['foto'] = $qad->foto;
						$sess_data['level'] = $qad->level;
						$sess_data['companyarea'] = $qad->companyarea;
						$this->session->set_userdata($sess_data);
					}
					header('location:'.base_url().'index.php/home');
			}
		}
		else
		{
			$this->session->set_flashdata('result_login', '<br>Username or Password incorrect');
			header('location:'.base_url().'index.php/login');
		}
	}
	
	public function getLoginDataB($usr,$psw,$loc)
	{
		$u = mysql_real_escape_string($usr);
		$p = md5(mysql_real_escape_string($psw));
		$q_cek_login = $this->db->get_where('admins', array('username' => $u, 'password' => $p, 'companyarea' => $loc));
		$domain = base_url();
		setcookie('username', $usr, time()+3600*8, '/', $domain);
		setcookie('password', $psw, time()+3600*8, '/', $domain);
		setcookie('companyarea', $loc, time()+3600*8, '/', $domain);
		
		if(count($q_cek_login->result())>0)
		{
			foreach($q_cek_login->result() as $qck)
			{
					foreach($q_cek_login->result() as $qad)
					{
						$text = "SELECT A.start_date,A.end_date FROM payroll_period A where A.`status`='1'";
						$dataperiod = $this->app_model->manualQuery($text);
						$resultperiod = $dataperiod->row();
						$sess_data['start_date'] = $resultperiod->start_date;
						$sess_data['end_date'] = $resultperiod->end_date;
						
						$sess_data['logged_in'] = 'aingLoginYeuh';
						$sess_data['username'] = $qad->username;
						$sess_data['nama_lengkap'] = $qad->nama_lengkap;
						$sess_data['foto'] = $qad->foto;
						$sess_data['level'] = $qad->level;
						$sess_data['companyarea'] = $qad->companyarea;
						$this->session->set_userdata($sess_data);
					}
					header('location:'.base_url().'index.php/home');
			}
		}
		else
		{
			$this->session->set_flashdata('result_login', '<br>Username or Password incorrect');
			header('location:'.base_url().'index.php/login');
		}
	}
	
	function get_lokasi() {
		$query = $this->db->get('lokasi');
		return $query;
	}
	
	public function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return $this->getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
	}
	
	
}
	
/* End of file app_model.php */
/* Location: ./application/models/app_model.php */